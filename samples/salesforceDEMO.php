<?php
// session_start() has to go right at the top, before any output!
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flock New Customer Sample</title>
        <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
    <h3> Creating an Account in Salesforce from PHP toolkit </h3>
    	
        <?php
	        
            require_once ('../soapclient/SforcePartnerClient.php');
            require_once ('../soapclient/SforceEnterpriseClient.php');

            define("USERNAME", "yourusername");
            define("PASSWORD", "yourpassword");
            define("SECURITY_TOKEN", "yourtoken");
    
                

            $mySforceConnection = new SforceEnterpriseClient();
            $mySforceConnection->createConnection("../soapclient/enterprise.wsdl.xml");

            // Simple example of session management - first call will do
            // login, refresh will use session ID and location cached in
            // PHP session
            if (isset($_SESSION['enterpriseSessionId'])) {
                $location = $_SESSION['enterpriseLocation'];
                $sessionId = $_SESSION['enterpriseSessionId'];

                $mySforceConnection->setEndpoint($location);
                $mySforceConnection->setSessionHeader($sessionId);

                
            } 
            else {
                $mySforceConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);

                $_SESSION['enterpriseLocation'] = $mySforceConnection->getLocation();
                $_SESSION['enterpriseSessionId'] = $mySforceConnection->getSessionId();
            }

            //echo "<h5>Salesforce.com SessionID:".$mySforceConnection->getSessionId()."</h5>";

            //----Creating a new Account with hardcoded RecordType
            $accounts = array();
            $accounts[0] = new stdclass();
            $accounts[0]->Name = 'API_account';
            $accounts[0]->Phone = '4153500714';
            $accounts[0]->RecordTypeId = '012F0000000qhymIAA';
            $accounts[0]->Website = 'wwww.cloudmobile.com.mx';

            $Accountid = '';

            try{
                $response = $mySforceConnection->create($accounts, 'Account');

                $ids = array();
                foreach ($response as $i => $result) {
                    array_push($ids, $result->id);                    
                    $Accountid = $result->id;
                    echo 'Account Created with ID:'.$result->id;
                }
            }
            catch(Exception $e){
                echo 'An error has ocurred while creating an Account: ' .$e->getMessage();
            }


            //------CREATING A Contact ------
            $contactRecord = array();
            $contactRecord[0] = new stdclass();
            $contactRecord[0]->FirstName = 'API_contact';
            $contactRecord[0]->LastName = 'API_contactLastName';
            $contactRecord[0]->AccountId = $Accountid;

            $responseContact = $mySforceConnection->create($contactRecord, 'Contact');

            $ids = array();
            foreach ($responseContact as $i => $result) {
                array_push($ids, $result->id);
            }

            $testDateEl = '2014-05-19T19:51:12.174Z'; // Here we need to create a new date with this formar otherwise it will not work
            
            $OpportunityId='';

            $opportunityRecord = array();
            $opportunityRecord[0] = new stdclass();
            $opportunityRecord[0]->Name = 'EMORAN_OPPDEMO';
            $opportunityRecord[0]->AccountId = $Accountid;
            $opportunityRecord[0]->StageName = 'Closed Won';
            $opportunityRecord[0]->CloseDate = $testDateEl;

            $responseContact = $mySforceConnection->create($opportunityRecord, 'Opportunity');

            $ids = array();
            foreach ($responseContact as $i => $result) {
                array_push($ids, $result->id);
                 $OpportunityId = $result->id;
            }

            //Opportunity Line item
            $opportunityLineItems = array();
            $opportunityLineItems[0] = new stdclass();
            $opportunityLineItems[0]->OpportunityId = $OpportunityId;
            $opportunityLineItems[0]->Quantity = 1;
            $opportunityLineItems[0]->PricebookEntryId = '01uF000000KbxqdIAB';
            $opportunityLineItems[0]->UnitPrice = 200.00;

            $responseContact = $mySforceConnection->create($opportunityLineItems, 'OpportunityLineItem');

            //----DISPLAYING INFO
            $query = "SELECT Id, Name from Account where Id = '$Accountid'";
            $response = $mySforceConnection->query($query);

             echo "<p class=\"bg-primary\"><h2>Created Account</h2></p>";

            echo "<table class=\"table table-striped\">";
           
            echo "<th>SF Id</th>";
            echo "<th>Customer ID</th>";
            
            foreach ($response->records as $record) {
                echo "<tr>";
                echo "<td>";
                echo "<b>".$record->Id."</b>";
                echo "</td>";
                echo "<td>";
                echo "<h4>".$record->Name."</h4>";
                echo "</td>";
                echo "</tr>";
            }
           
            echo "</table>";

            $queryContact = "SELECT Id, FirstName,LastName from Contact where AccountId = '$Accountid'";
            $response2 = $mySforceConnection->query($queryContact);

            echo "<h2>Created Contact</h2>";

            echo "<table class=\"table table-striped\">";
           
            echo "<th>SF Id</th>";
            echo "<th>Customer ID</th>";
            
            foreach ($response2->records as $record) {
                echo "<tr>";
                echo "<td>";
                echo "<b>".$record->Id."</b>";
                echo "</td>";
                echo "<td>";
                echo "<h4>".$record->FirstName." ".$record->LastName."</h4>";
                echo "</td>";
                echo "</tr>";
            }
           
            echo "</table>";

            //====== OPPORTUNITIES

            $queryOPP = "SELECT Id,Name from Opportunity where AccountId = '$Accountid'";
            $response2 = $mySforceConnection->query($queryOPP);

            echo "<h2>Created Opportunity</h2>";

            echo "<table class=\"table table-striped\">";
           
            echo "<th>SF Id</th>";
            echo "<th>Customer ID</th>";
            
            foreach ($response2->records as $record) {
                echo "<tr>";
                echo "<td>";
                echo "<b>".$record->Id."</b>";
                echo "</td>";
                echo "<td>";
                echo "<h4>".$record->Name."</h4>";
                echo "</td>";
                echo "</tr>";
            }
           
            echo "</table>";


            //====== OPPORTUNITY LINE ITEMS

            $queryitem = "Select Id from OpportunityLineItem where OpportunityId = '$OpportunityId'";
            $responseoli = $mySforceConnection->query($queryitem);

            echo "<h2>Created Opportunity Line Item</h2>";

            echo "<table class=\"table table-striped\">";
           
            echo "<th>SF Id</th>";
            echo "<th>Customer ID</th>";
            
            foreach ($responseoli->records as $record) {
                echo "<tr>";
                echo "<td>";
                echo "<b>".$record->Id."</b>";
                echo "</td>";
                echo "</tr>";
            }
           
            echo "</table>";
            

        ?>
    </body>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../assets/js/bootstrap.min.js"></script>
</html>